#! /bin/bash

wget https://sourceware.org/pub/bzip2/bzip2-latest.tar.gz

tar xvzpf bzip2-latest.tar.gz

cd bzip2-1.0.8

make install CFLAGS='-fPIC' CXXFLAGS='-fPIC'
