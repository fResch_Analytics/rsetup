#! /bin/bash
dnf update
dnf install -y glibc-devel.i686

curl -L 'www.openbugs.net/w/OpenBUGS_3_2_3?action=AttachFile&do=get&target=OpenBUGS-3.2.3.tar.gz' --output OpenBUGS-3.2.3.tar.gz

tar zxvf OpenBUGS-3.2.3.tar.gz
cd OpenBUGS-3.2.3

./configure
make
make install
