#! /bin/bash
zypper def
zypper install -y glibc-devel-32bit gcc-32bit

curl -L 'www.openbugs.net/w/OpenBUGS_3_2_3?action=AttachFile&do=get&target=OpenBUGS-3.2.3.tar.gz' --output OpenBUGS-3.2.3.tar.gz

tar zxvf OpenBUGS-3.2.3.tar.gz
cd OpenBUGS-3.2.3

./configure
make
make install
