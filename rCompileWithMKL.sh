#! /bin/bash

source /home/bobby/intel/mkl/bin/mklvars.sh intel64
cd /home/bobby/Downloads/R-3.6.2
./configure --enable-R-shlib --enable-threads=posix --with-lapack --with-blas="-fopenmp -m64 -I$MKLROOT/include -L$MKLROOT/lib/intel64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lpthread -lm"
make
sudo make install
