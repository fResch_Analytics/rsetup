#! /bin/bash

cd /home/rresch/Downloads/R-3.6.1

./configure --prefix=/opt/R/3.6.1 --enable-R-shlib --with-blas --with-lapack
make
make install
