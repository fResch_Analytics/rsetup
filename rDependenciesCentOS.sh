#! /bin/bash

dnf update

wget http://download.opensuse.org/repositories/home:/cornell_vrdc/Fedora_24/x86_64/jags4-devel-4.3.0-67.1.x86_64.rpm

dnf install -y gcc gcc-c++ gcc-gfortran gcc-go java-1.8.0-openjdk openssh curl \
java-1.8.0-openjdk-devel make ncurses-devel libquadmath-devel libstdc++-devel \
readline-devel libX11-devel libXt-devel libicu-devel openssl openssl-devel git \
libcurl-devel zlib-devel bzip2 bzip2-devel tcl-devel tk-devel xz-devel libxml2-devel \
pcre-devel libpng-devel libjpeg-turbo-devel libtiff-devel gsl-devel texlive \
compat-openssl10 unixODBC-devel blas lapack jags4-devel-4.3.0-67.1.x86_64.rpm
