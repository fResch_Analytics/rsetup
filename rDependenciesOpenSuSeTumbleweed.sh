#! /bin/bash
zypper ref
zypper dup

#wget http://download.opensuse.org/repositories/home:/cornell_vrdc/Fedora_24/x86_64/jags-1.0.4-7.1.x86_64.rpm
#wget http://download.opensuse.org/repositories/home:/cornell_vrdc/Fedora_24/x86_64/jags-devel-1.0.4-7.1.x86_64.rpm

zypper --no-gpg-checks install -y gcc gcc-c++ gcc-fortran gcc-go java-11-openjdk openssh curl \
java-11-openjdk-devel make ncurses-devel libquadmath0 libstdc++-devel cmake \
readline-devel libX11-devel libXt-devel libicu-devel openssl openssl-devel git \
libcurl-devel zlib-devel tcl-devel tk-devel xz-devel libxml2-devel zeromq-devel \
pcre-devel libpng16-devel libjpeg62-devel libtiff-devel gsl-devel texlive gdal\
unixODBC-devel blas-devel lapack-devel nlopt-devel gdal-devel proj proj-devel sqlite3-devel\
jags-1.0.4-7.1.x86_64.rpm #jags-devel-1.0.4-7.1.x86_64.rpm
