#! /bin/bash

apt-get update
apt-get upgrade -y

apt-get install -y gcc g++ gfortran gccgo openjdk-8-jdk openjdk-8-jre ssh curl \
make libncurses5-dev libquadmath0 libstdc++-8-dev libssl-dev \
libreadline-dev libx11-dev libxt-dev libicu-dev openssl git \
libcurl4-openssl-dev zlib1g-dev bzip2 libbz2-dev tcl-dev tk-dev xz-utils libxml2-dev \
libpcre2-dev libpng-dev libjpeg-dev libtiff-dev libgsl-dev texlive \
unixodbc-dev libblas-dev liblapack-dev jags
